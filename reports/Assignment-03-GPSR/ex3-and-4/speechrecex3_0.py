from datetime import datetime
import basebehavior.behaviorimplementation
import time
from JSGFParser import JSGFParser



class SpeechRecEx3_x(basebehavior.behaviorimplementation.BehaviorImplementation):
    # read locations from a file
    def read_stored_locations(self, fileName):
	    self.locations = []
	    try:
	        fileHandle = open(fileName, 'r')
	    except:
	        print "Cannot open location file " + fileName
	        return            
	    firstLineSkipped = False
	    for line in fileHandle.readlines():
	        line = line.rstrip("\n")    # remove endline
	        # Skip first line with keys
	        if not firstLineSkipped:
	            firstLineSkipped = True
	            continue             
	        # Read location
	        values = line.split(',')            
	        # Skip when line does not contain 4 arguments
	        if len(values) != 4:
	            continue                
	        # Skip comment lines
	        if len(values[0]) > 0 and values[0][0] == '#':
	            continue                
	        # location name
	        self.locations.append(values[0])
	        print "Location loaded: " + values[0]
	    fileHandle.close()

    def implementation_init(self):
        self.parser = JSGFParser('/home/student/sudo/brain/src/speech/hark-sphinx/grammar/michael_yannik.gram')
        print "[behavior] Initializing Example Behavior"
        self.last_recogtime         = time.time()
        self.last_speech_obs        = None  
        self.new_speech_obs         = False  
        
        self.state = "idle"
        self.navigating = False
        self.goto_movebase = self.ab.GotoMoveBase({'fileLocations':"/brain/data/locations/cafe_room.dat"})
        self.selected_behaviors = []
        self.locations = []
        self.read_stored_locations('/home/student/sudo/brain/data/locations/cafe_room.dat')
        print self.locations


    def update_last_speech_command(self):

        # sets the new_command boolean and sets the last understood speech_observation
        if (self.m.n_occurs('voice_command') > 0):
            
            (recogtime, obs) = self.m.get_last_observation("voice_command")

            if not obs == None and recogtime > self.last_recogtime:
                print "[observation] = ",obs
                self.last_speech_obs = obs
                self.last_recogtime = recogtime
                self.new_speech_obs = True
            else:
                self.new_speech_obs = False

    # remove optional items from input
    def remove_optionals(self, input):
    	return self.parser.filterOptionals(input)

    # set navigation goal
    def set_goal(self, goal_name):
    	self.goto = self.ab.gotowrapper({
            'align_to_goal': False,
            'error_range': -1,  # in meters
            'goal': goal_name
    	})
    	# only do this once
        if len(self.selected_behaviors) < 2:
            self.selected_behaviors.append(("goto_movebase", "True"))
            self.selected_behaviors.append(("goto", "self.navigating == True"))


    def navigate_to(self, goal_name):
        self.set_goal(goal_name)
        self.navigating = True

    # process speech input that fits <command>: 'alice [please] ...'
    def interpret_command(self, msg_split):
        first = msg_split[0]
        if(first != 'approach'):			# 'go', 'navigate', 'move',
            second = msg_split[1];          
                                            # 'kitchen', 'work', 'living'
            if(second in ['kitchen', 'work', 'living']):
        		if(second == 'kitchen'):
        			self.navigate_to('kitchen')
        			print "going to the kitchen"
        		elif(second == 'work'):
        			print "going to the work space"
        			self.navigate_to('work room')
        		else:
        			print "going to the living room"
        			self.navigate_to('living room')
            else: 
        		print "invalid destination"
        else: # "approach the dining table"
        	print "Approching the dining table. Could run subbehavior here"


    def interpret_question(self, msg):
        if(msg == 'what time is it'):
            print "It is " + datetime.now().strftime('%H:%M:%S') + " now"
        elif(msg == 'what is oldest most widely used drug on earth'):
            print "Coffee"
        elif(msg == "who are your creators"):
            print "Michael and Yannik"
        else:
            # later we can ask the speaker to repeat the question
            print "I did not understand this question"

    # process speech input
    def interpret_msg(self, msg):
        words = msg.split();
        # 'alice' and 'please' are removed (since they are optionals)
        if(words[0] in ['go', 'navigate', 'move', 'approach']):
            self.interpret_command(words)
        elif(words[0] in ['what', 'who']):
            self.interpret_question(msg)
        else: 
            print "invalid request"


    def implementation_update(self):
        self.update_last_speech_command()
        if self.new_speech_obs:
            print "[behavior] I heard:", self.last_speech_obs['message']
            short_msg = self.remove_optionals(self.last_speech_obs['message'])
            print short_msg
            self.interpret_msg(short_msg)
