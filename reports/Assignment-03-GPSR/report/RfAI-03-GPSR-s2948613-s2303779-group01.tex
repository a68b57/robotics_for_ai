\documentclass[a4paper,11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{parskip}
\usepackage{float}
\usepackage{amsmath}
\usepackage{caption}	% for multiline captions
\usepackage[shortlabels]{enumitem}
\usepackage[breaklinks=true]{hyperref}
\usepackage{listings}
\lstset{
    frame=single,
    breaklines=true,
    basicstyle=\footnotesize\ttfamily,
}



\input{boilerplate.tex}


\begin{document}

\begin{center}
    \huge{03 - GPSR}\\[1cm]
    \large{Group 01}\\[0.5cm]
    \large{
        Mingcheng Ding (s2948613)\\
        \texttt{dingmingcheng@163.com}
    }\\[0.5cm]
    \large{
        Yannik Beckersjurgen (S2303779)\\
        \texttt{y.beckersjurgen@student.rug.nl}
    }\\[0.5cm]
    \large{
        \textbf{Robotics for AI}\\
        University of Groningen\\[0.5cm]
    {\today}}\\[0.5cm]
\end{center}


% \begin{figure}[!htb]
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{figures/fractal}
% 	\caption{A caption}
% 	\label{fig:fractal}
% \end{figure}

\section{Exercise 1 - Sound Separation}
\label{sec:ex1}
For the foreground signal, we kept a distance of about 50 cm from the kinect. In order to avoid the incorrect splitting in the middle of a voice command, we used relatively long pauses between each utterance (compared to the pause between e.g. "pseudo/alice" and the actual command). 

In this assignment, we used HARK, which provides source localization and separation modules for speech recognition. The parameters that we adjusted for splitting the mixed foreground signals from the background noises are listed as below: 

\begin{lstlisting}
SEPARATE_GAIN = 50
TRACK_PAUSE = 1000
TRACK_THRESH = 31.0
UPPER_BOUND_FREQUENCY = 4000 # in GHDSS node
LOWER_BOUND_FREQUENCY = 300  # in GHDSS node
\end{lstlisting}

We initially applied the default setting of the parameters. It turned out that the volume of the audio was very low. Then our first adjustment was the \verb|SEPARATE_GAIN|. It increases the gain of the source before recognition. We achieved an appropriate volume of the recording when the \verb|SEPARATE_GAIN| is set 50. However, this increases the volume of the background noise to the same extent. In our next step, we tried to remove some this background noise. We noticed that there was a high-pitched beep in each recording. Its frequency is much higher than the vocal sentences, therefore, we reduced the \verb|UPPER_BOUND_FREQUENCY| to 4000 HZ in order to eliminate the beep noises. Similarly, we slightly increased the \verb|LOWER_BOUND_FREQUENCY| of the sound such that the low pitch part of the background will be cancelled. We used this frequency range because it corresponds to the frequency spectrum of human speech.

The \verb|TRACK_PAUSE| sets the duration for which sources can be silent before the source is no longer tracked. A longer track pause leads to the less number of separated sources and within the source, the background noise will continue some time after the vocal command is finished, while a low value of the \verb|TRACK_PAUSE| parameter will cause the unexpected splits in a recording and even very short pauses will be considered to be a new source. We got at least 10 good separations from the original sources with the \verb|TRACK_PAUSE| at 1000.

The \verb|TRACK_THRESH| parameter determines the minimum sound volume that determines whether a source will be tracked. If it is set to too high, only the high-amplitude component of the signal will be tracked. This may result in cutting off relatively quiet parts of the speech signal. For example, in the word 'bedroom', the 'room' might not be loud enough. On the other hand, if this parameter is set too low, more background noise (which is usually more quiet than the the speech signal) will be tracked. We followed the default value for this exercise because all words in the utterance are clearly presented in the splitted files.    





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exercise 2 - Speech Recognition}
\label{sec:ex2}

In addition to HARK, we used Pocketsphinx for this exercise, which provides speaker-independent continuous speech recognition.

We changed the following lines in the file \verb|wav_recog.n|:
First, in the configuration of the subnet network, we set these parameters:
\begin{lstlisting}
      <Parameter name="TRACK_THRESH" type="float" value="27.0"/>
      <Parameter name="SEPARATE_GAIN" type="float" value="5.0" />
      <Parameter name="TRACK_PAUSE" type="float" value="1000"/>
\end{lstlisting}

Second, we changed the bounds of the frequency filter of the GHDSS node:
\begin{lstlisting}
      <Parameter name="LOWER_BOUND_FREQUENCY" type="int" value="300"/>
      <Parameter name="UPPER_BOUND_FREQUENCY" type="int" value="4000"/>
\end{lstlisting}

We ran the \verb|decode_all_mixes| script on our mixed signals. The performance for the different locations is shown in \autoref{tab:mixes}. The overall mean was 0.573 (N = 100, SD = 0.2). We tried several different values for the three parameters \verb|TRACK_THRESH|, \verb|SEPARATE_GAIN|, and \verb|TRACK_PAUSE|. We noticed that the \verb|SEPARATE_GAIN| has only a very small effect on the overall performance. In contrast, the \verb|TRACK_THRESH| parameter does have a significant effect. We saw that we could significantly improve the performance on some of these mixes in isolation, but this was often associated with a decreased performance for other locations. 

\begin{table}[]
\centering
\caption{Exercise 2 - Performance on mixed signals}
\label{tab:mixes}
\begin{tabular}{ll}
Mix         & Performance \\ \hline
aula 1      & 0.586       \\
aula 3      & 0.494       \\
aula 4      & 0.657       \\
cantine 1   & 0.740       \\
lecture 1   & 0.678       \\
lecture 2   & 0.778       \\
practical 1 & 0.566       \\
repro 1     & 0.237       \\
robotlab 1  & 0.673       \\
slack 1     & 0.686       \\ \hline
mean        & 0.573      
\end{tabular}
\end{table}


The effect of the grammar file is quite significant. There are mainly two aspects. Firstly, the grammar file largely contributes to the improvement of the score of the recognition (see \autoref{lst:label1}). We compared the score from the \verb|practical\_1| and \verb|lecture\_2| directory. The performance on these files is reduced by about 0.5 (50\%) when the grammar file is not used for the sound recognition. The score of \verb|practical\_1| dropped from 56\% to 8.9\% while the \verb|lecture\_2| decreased to from 68\% to 18.3\%. Since decoding the signals takes quite a long time when no grammar is used, we did not compare the performance for all mixes. Nevertheless, since the two scenarios which we did test showed such a marked decrease in performance, we anticipate that their performance will drop similarly. Thus, in addition to strongly increasing the recognition performance, using the grammar file also resulted in much faster decoding.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exercise 3 - Behaviour Integration}
\label{sec:ex3}

We will discuss the the behavior implementation in \autoref{sec:ex4} since the implementation is the same for exercise 3 and 4.
The JSpeech Grammar Format (JSGF) is a file format for grammars used in speech recognition to define which kind of speech a computer should expect or recognize. By using application dependent grammars, the accuracy of the speech recognition can be increased and we can make sure that the speech input is valid with respect to the rules defined in the grammar. Our grammar file is shown in \autoref{lst:grammar}. 

\lstinputlisting[caption = {michael\_yannik.gram}, label = {lst:grammar}]{../ex3-and-4/michael_yannik.gram}

We chose a grammar in which a correct sentence is either a command or a question. \\

\noindent \textbf{Commands} always begin with 'alice', which may be followed by a 'please'. Then, the actual command follows, which can be either
\verb|<approach>| or \verb|<moveverb> [to] [the] <location>|, where \verb|<approach>| is short for \verb|approach the dining table|. We decided to treat the 'approach' command differently since it should start a sub-behavior, whereas the other commands all initiate navigation. In addition, the 'dining table' is an object and not a location. If similar commands were to be added, we could introduce the term \verb|<object>|. We defined \verb|<moveverb| as either \verb|go|, \verb|navigate|, or \verb|move| and \verb|<location>| to be one of the three used in this exercise. Note that we decided to make \verb|to| and \verb|the| optional since the meaning of the sentence is also clear if they are not present.\\

\noindent \textbf{Questions}
Questions may be preceded by 'alice'. We decided to split the questions into \verb|<whoquestion>| and \verb|<whatquestion>| for generality, although this is not strictly necessary for this exercise. In each of these, we simply list the question(s) that can be asked.\\

\noindent Overall, we designed the grammar in such a way that all valid instances of the sentences can be recognized. We tested this with the keyboard input and did not find a command that was not recognized. We tried to make the recognition robust (for instance by making \verb|to| and \verb|the| optional for \verb|<command>|). This means that also sentences like \verb|alice go kitchen| will be recognized.

We will discuss the performance of the responses to text input and to real speech input in the next section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exercise 4 - Behaviour \& Speech in Simulation}
\label{sec:ex4}

Running instructions:
\begin{lstlisting}
roslaunch alice_gazebo alice_simulation.launch 		
roslaunch alice_nav_sim xtions.launch
roslaunch alice_nav_sim alice_nav.launch
# cd to brain/src directory
./start.sh config/config_speech_rec_ex3

# speech input
python speech/speech_to_memory.py 'alice please what is' 

# OR

# kinect input
python speech/SpeechRos.py --source kinect --gram michael_yannik.gram
\end{lstlisting}

When running \verb|alice_simulation.launch|, the appropriate map needs to be selected because this launch file is used in different assignments. In \verb|wav_recog.n|, we used the same parameters as for the previous exercise.

Our behavior implementation is shown in \autoref{lst:behavior}. Every time a new \verb|voice_command| is added to the memory module, we first remove the optional items using the function \verb|remove_optionals| which calls the corresponding function of the \verb|JSGFParser|. Next, the input is processed in order to respond correctly. The processing step is similar to the definition of our grammar. First, we call the function \verb|interpret_msg|, which determines whether the beginning of the command fits the \verb|<command>| or the \verb|<question>| structure and calls \verb|interpret_command| or \verb|interpret_question| accordingly. In the former, we determine the goal location and start the navigation. In the latter, we look up the question and print an appropriate response. During each of these steps we check whether the input is correct and provide feedback if the input is not valid. Note that when determining the navigation goal-locations, we only look at the first word of the location. That is to say, \verb|alice go to the living room| and \verb|alice go to the living <some other word>| and \verb|alice go to the living| would all be accepted as valid commands and the robot would start navigating to the living room. We chose this implementation in order to decrease the false negative rate of valid sentences that are not recognized. If different behavior was required, it would be trivial to change the code to match the full location name exactly.

We tested the behavior and the grammar using the text-input method. The behavior worked as expected and all sentences were handled correctly. The navigation works in simulation. After the the robot is given a navigation command, it navigates toward the goal destination. Whilst navigating, the robot still listens to other speech commands and we can give it a new goal location to approach. The robot will then cancel the current navigation and approach the new goal. When the speech input is provided using the \verb|kinect|, most speech commands are still understood correctly. 

Below we present an example output of the behaviour in the terminal, see \autoref{lst:navi_eg}. We gave a voice command to move to the kitchen, and the \verb|[observation]| shows the most probable input command. The line next to the \verb|[behavior]| shows the simplified command through the grammar file (navigate kitchen). As we can see in \autoref{fig:navi_eg}, in RVIZ, the navigation goal is thus set corresponding to the coordinate of the kitchen in the map file.

\begin{lstlisting}[caption={navigate to the kitchen},label={lst:navi_eg}]
[observation] =  {'2best': ['alice navigate to the kitchen', 'alice navigate to the kitchen'], 'message': 'alice navigate to the kitchen', 'azimuth': 0.153}
[behavior] I heard: alice navigate to the kitchen
navigate kitchen
['navigate', 'kitchen']
going to the kitchen
Should log precon here!
Logging precon for: GotoWrapper
Navigating towards 'kitchen'
Navigating towards {'y': 2.81, 'x': -2.004, 'angle': 90.0}
\end{lstlisting}

\begin{figure}
	\centering
 	\includegraphics[width=0.7\textwidth]{../ex3-and-4/navigate_to_kitchen.png}
 	\caption{Navigate to kitchen(RVIZ)}
 	\label{fig:navi_eg}
\end{figure}

We found that there are two kinds of errors for the kinect speech input. First, it happens sometimes that no speech command is recognized at all. Second, it happens that the system understands a different speech command than what is actually being said. We did not, however, observe cases in which the system believes to have heard something that is not consistent with the grammar.

We noticed some interesting peculiarities of the speech recognition. For example, the sentence \verb|what time is it| has a relatively large number of false positives, i.e. the speech recognition system believes to have heard this sentence, although the real input was different. We think this might be due to the fact that this sentence is shorter than the other sentences, which may increase its probability. We also saw that commands using \verb|go| where better recognized than \verb|move| or \verb|navigate|. We believe that this might be due to 'go' only having a single syllable and being quite easy to articulate clearly. We also noticed that speech commands containing the optional items 'alice' and 'please' are better recognized than the equally valid, but shorter versions in which they are omitted.

Below, we present another example in order to show how the behavior responds to a question:
\begin{lstlisting}
[observation] =  {'2best': ['what time is it', 'what time is it'], 'message': 'what time is it', 'azimuth': 0.153}
[behavior] I heard: what time is it
It is 15:58:05 now
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\appendix

% Exercise 4
\section{exercise 4}
\label{sec:code-ex4}
\lstinputlisting[style = code, label = {lst:behavior}, caption = {speechrecex3\_0.py}]{../ex3-and-4/speechrecex3_0.py}

\end{document}