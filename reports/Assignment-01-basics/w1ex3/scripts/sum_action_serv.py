#! /usr/bin/env python
import roslib
import rospy

import actionlib

import w1ex3.msg
from sensor_msgs.msg import Image

class SumAction(object):
    _result   = w1ex3.msg.PixelSumResult()  #  initialize the result
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, \ 
            w1ex3.msg.PixelSumAction, execute_cb=self.execute_cb, \ 
            auto_start = False)
        self._as.start()
        rospy.loginfo((rospy.get_caller_id() + ": Initialized"))
        
    def execute_cb(self, goal):
        pixels_col = goal.col.width * goal.col.height   
        pixels_grey = goal.grey.width * goal.grey.height
        self._result.sum = pixels_col + pixels_grey
        # rospy.loginfo('%s: Succeeded' % self._action_name)  
        self._as.set_succeeded(self._result)
      
if __name__ == '__main__':
    rospy.init_node('pixelsum')
    SumAction(rospy.get_name()) 
    rospy.spin() 
