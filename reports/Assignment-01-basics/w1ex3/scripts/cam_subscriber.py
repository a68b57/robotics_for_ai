#!/usr/bin/env python
import rospy
from w1ex3.srv import *
from sensor_msgs.msg import Image
import actionlib
import roslib
import w1ex3.msg
import cv2
from cv_bridge import CvBridge, CvBridgeError

def convert_image_cv(data, type):
    # use type = "8UC1" for grayscale image
    # use "bgr8" if its a color image  
    try:
        cv_image = bridge.imgmsg_to_cv2(data, type)
    except CvBridgeError, e:
        print e
    ### Show the image  
    #cv2.imshow("Camera Image", cv_image)
    #cv2.waitKey(1) # one will update, 0 will wait for keyboard info
    return cv_image


# in case of new camera image from kinect
def callback(data):
    rospy.wait_for_service('rgb_to_grey')   
    result = greyImg(data) # for using it use result.out (I think)
    grey = result.out

    # convert and display image
    color_cv = convert_image_cv(data, "bgr8")
    grey_cv = convert_image_cv(grey, "8UC1")
    cv2.imshow("Color Image", color_cv)
    cv2.waitKey(1)
    cv2.imshow("Grey Image", grey_cv)
    cv2.waitKey(1)

    # compute sum of pixels
    pixel_sum_result = action_client(data, grey)
    pixel_sum = pixel_sum_result.sum
    rospy.loginfo((rospy.get_caller_id() + "Pixel Sum: %d") % pixel_sum)
    

# subscribe to kinect input
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/kinect2/hd/image_color", Image, callback)
    rospy.spin()

# request pixel sum calculation from action server
def action_client(col, grey):
    client = actionlib.SimpleActionClient('pixelsum', w1ex3.msg.PixelSumAction)
    client.wait_for_server()
    goal = w1ex3.msg.PixelSumGoal(col, grey);
    client.send_goal(goal)
    client.wait_for_result()
    return client.get_result()

if __name__ == '__main__':
    try:
        greyImg = rospy.ServiceProxy('rgb_to_grey', RgbToGrey)
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e
    bridge = CvBridge()
    listener()