#!/usr/bin/env python
import rospy
from w1ex3.srv import *
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy


## convert from sensor_msg format to opencv format (Numpy)
def convert_image_cv(data, type = "bgr8"):
    # use type = "8UC1" for grayscale image
    # use "bgr8" if its a color image  
    try:
        cv_image = bridge.imgmsg_to_cv2(data, type)
    except CvBridgeError, e:
        print e   
    
    return cv_image
    
## convert color cv image to grey cv image
def convert_cv_to_grey(cv_image):
	res = cv2.cvtColor(cv_image, cv2.COLOR_RGB2GRAY)
	return res
    
    
## Convert from opencv format to sensor_msgs format
def convert_image_ros(data, type = "8UC1"):
    # use type = "8UC1" for grayscale image
    # use "bgr8" if its a color image
    try:
        ros_image = bridge.cv2_to_imgmsg(data, type)
    except CvBridgeError, e:
        print e
    return ros_image

## handles conversion requests by clients
def handle_rgb_to_grey(req):
	cv_img = convert_image_cv(req.inp, "bgr8")
	grey_cv = convert_cv_to_grey(cv_img)
	grey_img = convert_image_ros(grey_cv, "8UC1") 
	return grey_img
	
## setup server    
def rgb_to_grey_server():
    rospy.init_node('rgb_to_grey_server')
    s = rospy.Service('rgb_to_grey', RgbToGrey, handle_rgb_to_grey)
    rospy.loginfo((rospy.get_caller_id() + ": Initialized"))
    rospy.spin()

if __name__ == "__main__":
    bridge = CvBridge()
    rgb_to_grey_server()
