import basebehavior.behaviorimplementation


class NavigateEx3_0(basebehavior.behaviorimplementation.BehaviorImplementation):

    # for generality - so we don't need to hardcode the goal/location names
    # adapted from gotomovebase_1.py, original function also reads the coordinates
    def read_stored_locations(self, fileName):
        self.locations = []
        try:
            fileHandle = open(fileName, 'r')
        except:
            print "Cannot open location file " + fileName
            return            
        firstLineSkipped = False
        for line in fileHandle.readlines():
            line = line.rstrip("\n")    # remove endline
            # Skip first line with keys
            if not firstLineSkipped:
                firstLineSkipped = True
                continue             
            # Read location
            values = line.split(',')            
            # Skip when line does not contain 4 arguments
            if len(values) != 4:
                continue                
            # Skip comment lines
            if len(values[0]) > 0 and values[0][0] == '#':
                continue                
            # location name
            self.locations.append(values[0])
            print "Location loaded: " + values[0]
        fileHandle.close()

    def implementation_init(self):

        self.state = "idle"
        print "NavigateEx3 initialized"
        self.navigating = False
        self.goto_movebase = self.ab.GotoMoveBase({'fileLocations':"/brain/data/locations/locations-dynamic.dat"})      # relative path: should start with /brain/data/locations...
        self.selected_behaviors = []
        self.try_count = 1
        self.locations = []
        self.goal_number = 0;
        self.read_stored_locations('/home/student/sudo/brain/data/locations/locations-dynamic.dat')     # absolute path: /home/student/sudo/brain/data/locations...
        print self.locations

    def set_goal(self, goal_name):
        self.goto = self.ab.gotowrapper({
                'align_to_goal': False,
                'error_range': -1,  # in meters
                'goal': goal_name
        })

    def navigate_to(self, goal_name):
        self.set_goal(goal_name)
        self.navigating = True

    def handle_state(self):
        if self.goto.is_finished():
            if self.goal_number >= len(self.locations) - 1:
                self.set_finished()
            else: 
                self.goal_number += 1
                next_goal = self.locations[self.goal_number];
                self.navigate_to(next_goal)
                print "Going to next goal: " + next_goal
        if self.goto.is_failed():
            if self.try_count >= 3:
                if self.goal_number == len(self.locations) - 1:       # this was last goal, finish
                	self.set_finished()
                	return
                self.goal_number += 1
                next_goal = self.locations[self.goal_number];
                print "Tried 3 times. Going to next goal: " + next_goal
                self.navigate_to(next_goal)
                self.try_count = 1                       # new goal, give it 3 tries again

            else :
                self.try_count = self.try_count + 1
                self.navigate_to(self.locations[self.goal_number])
                print "Trying again: Attempt %d" % self.try_count



    def implementation_update(self):
        
        if self.state == "idle":
            self.set_goal(self.locations[self.goal_number])
            self.selected_behaviors.append(("goto_movebase", "True"))
            self.selected_behaviors.append(("goto", "self.navigating == True"))
            self.navigating = True
            self.state = "navigating"

        self.handle_state()