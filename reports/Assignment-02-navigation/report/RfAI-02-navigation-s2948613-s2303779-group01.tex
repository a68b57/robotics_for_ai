\documentclass[a4paper,11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{parskip}
\usepackage{float}
\usepackage{amsmath}
\usepackage{caption}	% for multiline captions
\usepackage[shortlabels]{enumitem}
\usepackage[breaklinks=true]{hyperref}
\usepackage{listings}
\lstset{
    frame=single,
    breaklines=true,
}


\input{boilerplate.tex}

\begin{document}
\begin{center}
    \huge{02 - Navigation}\\[1cm]
    \large{Group 01}\\[0.5cm]
    \large{
        Mingcheng Ding (s2948613)\\
        \texttt{dingmingcheng@163.com}
    }\\[0.5cm]
    \large{
        Yannik Beckersjurgen (S2303779)\\
        \texttt{y.beckersjurgen@student.rug.nl}
    }\\[0.5cm]
    \large{
        \textbf{Robotics for AI}\\
        University of Groningen\\[0.5cm]
    {\today}}\\[0.5cm]
\end{center}


% \begin{figure}[!htb]
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{figures/fractal}
% 	\caption{A caption}
% 	\label{fig:fractal}
% \end{figure}

\section{Exercise 1 - Learning to map and localize}

\subsection{Creating the map (part I)}
% Make a launch file that runs the Gmapping and our teleoperation module
% In your report, mention the required transforms and topics for Gmapping to work properly.
Running instructions:
\begin{lstlisting}
roscore
roslaunch alice_gazebo alice_simulation.launch (open the world)
rosrun rvis rvis 
roslaunch 02_navigation_ex1 02_nav_ex1.launch
# to save the map after exploring the environment:
rosrun map_server map_saver -f <mapName> 
\end{lstlisting}
%http://wiki.ros.org/gmapping
The launch file is shown in \autoref{lst:ex1-mapping} on page \pageref{lst:ex1-mapping}.
We launch a \verb|slam_gmapping| node in order to map the environment and localize at the same time (simultaneous localization and mapping, SLAM). This implementation is based on particle filters and uses laser scans as the observation source. 

Nodes of this type subscribe to the topic \verb|tf| (tf/tfMessage), which provides the transforms between the laser, the base, and the odometry in order for the node to work properly. In particular, a transform form the frame attached to incoming scans to the \verb|base_link| is needed and we need a transformation from \verb|base_link| to the odometry (odom).
In addition, the nodes subscribes to the \verb|scan| (sensor\_msgs/LaserScan)  topic since the laser is used to map the environment. 
With the \verb|args="scan:=scan"| parameter, we specify the name of the laser that is to be used for the mapping (in our case this is also 'scan'). Further we set the \verb|base_frame| parameter to \verb|base_link|. This is the default value of the parameter, but we set it explicitly in order to understand how the gmapping node is set up. The same holds true for the \verb|xmin|, \verb|xmax|, \verb|ymin|, \verb|ymax| parameters, which determine the initial map size. We also launch an \verb|alice_teleop_keyboard_node| so we can control the robot with the keyboard during the simulation.

\subsection{Localizing using the map (part II)}
%TODO: roslaunch alice_nav alice_config.launch only has to be used for real robot, so that could be left out
% see part II ( see instructions on nester)
\begin{lstlisting}
roscore
roslaunch alice_gazebo alice_simulation.launch
roslaunch alice_nav_sim alice_config.launch
# launch amcl node and teleop and the map server:
roslaunch 02_navigation 02_nav_ex1_amcl.launch
\end{lstlisting}

% Make a launch file that runs AMCL, teleoperation module, and the map that you just saved.
The launch file is shown in \autoref{lst:ex1-localize} on page \pageref{lst:ex1-localize}. We create an \verb|amcl| node which provides the localization functionality. Amcl provides probabilistic localization in two-dimensions. It uses a Monte Carlo localization method based on a particle filter to find the most likely position of the robot in the map (that is known beforehand).

As in part I, we specify the topic on which the laser observations are published ('scan'). Next, to provide the map, we create a \verb|map_server| node which makes the map (that we created in part I) available to other nodes. We again launch a teleoperation node to control the robot in the simulation.

We tested the localization in the Gazebo simulation and it worked as expected. For example, we 'kidnapped' the robot and moved it to a different position. We gave it a rough pose estimate (see the two top frames in \autoref{fig:ex1-kidnap}), after which we can see a cloud of pose particles. After moving the robot forward a small distance, the particles quickly converge to the base of the robot and the robot has an exact position estimate again. This is shown in the lower frames in \autoref{fig:ex1-kidnap}.\\

 \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.9\textwidth]{../ex1/combined.png}
 	\caption{Top frames: after 'kidnapping' and giving a pose estimate. \\ 
 			 Bottom frames: recovery and conversion after new observations.}
 	\label{fig:ex1-kidnap}
 \end{figure}



\section{Exercise 2}
Running instructions:
First choose the appropriate map in alice\_nav.launch (we use a different map in exercise 3).
\begin{lstlisting}
roscore
roslaunch alice_gazebo alice_simulation.launch
roslaunch xtions.launch
roslaunch alice_nav_sim alice_nav.launch
\end{lstlisting}


\subsection{Overview}
In this exercise, we are configuring the navigation stack and the move\_base package in particular. This package is an implementation of an action and provides navigation functionality by combining a global and a local planner. Each of these planners has its own costmap, based on which the path planning is executed. The launch file for this exercise and the corresponding configuration files are shown in \autoref{sec:code-ex2} on page \pageref{sec:code-ex2}.

In the launch file, we create a map-server and an amcl node. Then we configure the move\_base node. The parameters that we used are listed below, in the same order as they occur in the assignment instructions. In particular, the global and the local planners have to be set up.

\subsection{Parameter descriptions and explanation}
% You have to describe the use of each parameter.
\begin{itemize}
\item Control Frequency of 5Hz: Parameter: \textbf{controller\_frequency}. This is the frequency with which velocity commands are sent to the base by the controller. By using 5 Hz, the commands are sent five times per second.

\item Move\_base should give up after 5 seconds if it cannot find a path: Parameter: \textbf{planner\_patience}. This parameter determines how long the planner tries to find a plan before giving up and executing space-clearing operations. In our case, this is five seconds.

\item Move\_base should keep trying with local commands up to 10 seconds. Parameter: \textbf{controller\_patience}. This determines how long the controller waits  for valid control commands before performing space-clearing. In our case, this is 10 seconds.

\item Robot is not allowed to rotate on spot if it is stuck. Parameter: \textbf{clearing\_rotation\_allowed}. This Boolean parameter is set to true if the robot is allowed to rotate in order to recover from a position in which it is stuck. Otherwise, it is set to false. Note that we do not need to set this parameter explicitly since we did not add rotation to the custom list of recovery behaviours. For clarity however, we added it to the launch file nonetheless.

\item Robot should clear costmap after it is stuck. Parameter: \textbf{recovery\_behaviors}. The argument for this parameter is a list and determines which recovery behaviour plugins will be performed in case of the robot being stuck (i.e. failing to find a valid plan). To set this parameter, we load a .yaml including the behaviours that we want to have. In order to perform the clearing of the costmaps, we add \textbf{aggressive\_reset} and \textbf{conservative\_reset} to these behaviours.

\item Robot should use the the global\_planner as the base global planner. \\ Parameter: \textbf{base\_global\_planner}. This specifies the name of the global planner that will be used. In our case, this is \verb|global\_planner/GlobalPlanner|.

\item Robot should use the Dynamic Window Approach as local planner. \\Parameter: \textbf{base\_local\_planner}. This parameter sets the name of the local planner that will be used. We use the dynamic window approach planner \\ (\verb|dwa\_local\_planner/DWAPlannerROS|). This planner uses as local costmap to plan the kinematic trajectory to the goal (given a global plan by the global planner). It then sends the (dx,dy,d$\theta$) values to the the base of the robot. 

\item Robot should not update costmap when it is not moving. \\ Parameter: \textbf{shutdown\_costmaps}. This parameter specifies whether the costmaps are updated when move\_base is inactive. For this assignment, we set it to true to ensure that the costmaps will not be updated when the robot is not moving.

\item Robot should use a circular footprint for global planning and a convex footprint for local planner. The footprints for the global and for the local planner are set in \\ \verb|global_costmap_params.yaml| and \verb|local_costmap_params.yaml| respectively. The footprint represents the physical extension of the base of the robot. The planner need to take this into consideration in order to determine whether the robot is too large to traverse potential paths. For the global planner, a circular footprint is sufficient as this will speed up the planning since rotations of the robot do not have to be taken into account. We chose a \verb|robot_radius| of 0.4, with the center being base-link-dummy. For the local planner, we need a more precise footprint. The DWA planner requires that this footprint is convex. We opted for a rectangular footprint since this fits the real shape of Alice. Our footprint for the local planner is given by \\ \verb|footprint: [[0.2,0.30],[-0.57,0.30],[-0.57,-0.30],[0.20,-0.30]]|. The rectangle is slightly longer at the front of the robot in order to accommodate the laser sensor. The \verb|robot_base_frame| is set to \verb|base\_footprit|.

\item The Global planner footprint should be set in a way that Robot should be able to plan paths through tight corridors and doors. \\ We already know that the robot should have a circular footprint for the global planner. It is then a matter of tuning the radius such that it is small enough navigate through tight surroundings whilst also avoiding collision with objects and making sure that the cameras do not mistake the robot's own base as an object. From the simulation, a radius of 0.4 m seems to achieve this purpose.

\item Robot should use all the available sensors for local planning. The obstacle layer parameters that are shared by the global and by the local costmap for the xtions and for the laser are defined in \verb|costmap_common_params.yaml|. In \verb|local_costmap_params.yaml|, the remaining parameters are defined for the local cost map. In order to use all observation sources, we use both the xtion layer and the laser layer and add them to the \verb|plugins| section.

\item Robot may use any sensor for global planning. We followed the same steps as for the previous point, adding first the laser layer and then the xtion layer to the \verb|plugins|. We used the same settings as for the local planner. This means that the xtions will overwrite the laser if they provide conflicting information.

\item The 3D sensor in local planner has higher priority than the Laser. It should overwrite the laser.
In the \verb|plugins| section, the final costmap is built from top to bottom. This means that the top layers can be overwritten by layers below them (given that we use the overwrite combination method). Therefore, we first add the laser layer and below that the xtion layer. We set the \verb|combination_method| to 0 for the xtion layer, meaning that it will overwrite the laser observations.

\item The Footprint of the local planner should be convex representation of the real robot.
We use used a rectangle with vertices \verb|[[0.2,0.30],[-0.57,0.30],[-0.57,-0.30],[0.20,-0.30]]| relative to center, which we set to \verb|base_footprint|. The rectangle is a bit longer at the front in order to accommodate the laser.

\end{itemize}


\section{Exercise 3}
Running instructions:
\begin{lstlisting}
roscore
roslaunch alice_gazebo 3_doors.launch
roslaunch alice_nav_sim xtions.launch
roslaunch alice_nav_sim alice_nav.launch
python benchmark.py
./start.sh config/config_navigate_ex3 
\end{lstlisting}
For this exercise, we are going to implement our own behaviour architecture in the 3-doors environment. The goal of this simulation is to reach all the waypoints on the map. In addition, the robot should be able to re-plan the path if the current path is blocked by unexpected obstacles and the robot will have three attempts for reaching each waypoint. In case it does not succeed in reaching the waypoint after three attempts it should skip the current goal and navigate to the next one. 

Note that when the robot gets stuck in the map, the recovery behaviour should clear all the global cost-map. We tried both the \textbf{aggressive\_reset} and \textbf{conservative\_reset} with different \textbf{reset\_distance} values, but the cost map was not cleared even if we saw that there was a log in the terminal saying that the cost map is cleared.


 \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=\textwidth]{../ex3/01-start.png}
 	\caption{Set the pose estimate for the Alice in Rviz}
 	\label{fig:ex3-01}
 \end{figure}
In \textbf{Rviz}, the local cost-map is presented in cyan. The global cost-map is shown in a light cyan. The red arrow on the map is the goal of the robot. The pink curve is the path generated by the global planner. In \autoref{fig:ex3-01}, we set the pose estimate for Alice corresponding to its location in \textbf{Gazebo}. The local cost map will keep updating via the laser and front/back xtions. The same is true for the global cost map. This means that a after a change in the environment, the global cost map is only updated as soon as one of the sensors detects the change.    
  \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=\textwidth]{../ex3/02-path-before-door-closes.png}
 	\caption{Got a path to the waypoint1 when the middle dooe is still open}
 	\label{fig:ex3-02}
 \end{figure}
 
In \autoref{fig:ex3-02}, the robot set the waypoint1 as the goal and it was planned to pass through the middle door as it seems to be path associated with the lowest cost overall.
 
  \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=\textwidth]{../ex3/03-new-path-after-door-closed.png}
 	\caption{Middle doors closed, change the original plan}
 	\label{fig:ex3-03}
 \end{figure}
When the robot is approaching the door, the middle door was suddenly closed (see the right frame in \autoref{fig:ex3-03}). Both local and global cost-maps saw this change in time. The robot stopped moving as expected and quickly found an alternative path through the left door.
  \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=\textwidth]{../ex3/04-first-goald-reached.png}
 	\caption{First goal reached}
 	\label{fig:ex3-04}
 \end{figure}
 
Once the robot reached the first goal, the navigation goal thus was set to the next waypoint. (See \autoref{fig:ex3-04}). Because there are no obstacles on the path to the second waypoint, the robot reached the destination without interruption. 
 
  \begin{figure}[!htb]
 	\centering
 	\includegraphics[width=\textwidth]{../ex3/05-final-goal-reached.png}
 	\caption{Final goal reached}
 	\label{fig:ex3-05}
 \end{figure}
 
Eventually, the robot went around the ladder and the table and reached the final waypoint as shown in the \autoref{fig:ex3-05}. Thus, the robot was able to deal with the change in environment and successfully navigated toward the three waypoints. 

The python code of the behaviour is shown in \autoref{lst:ex3}. We wrote our own function \\ \textbf{read\_stored\_locations} to read the coordinates from the file and pass them to the implementation function through the list called \textbf{locations}. With regard to the number of attempts we declared the variable \textbf{try\_count} which counts the number of attempts for each goal. Once the robot reaches the navigation goal, this variable will be reset to 1 and the navigation goal is changed to the next goal in the \textbf{locations} list. Should the navigation to one waypoint not succeed, the variable \textbf{try\_count}  will be increased by 1 until it becomes larger than 3 and the navigation goal remains the same during this process. If the robot still does not reach the goal, the current goal will be skipped and the robot tries to reach the next goal in the list.


\clearpage
\section*{Appendix: source code}
\subsection*{exercise 1}
% Exercise 1
\lstinputlisting[style = code, label = {lst:ex1-mapping}, caption = {Ex. 1: 02\_nav\_ex1.launch}]{../ex1/02_nav_ex1.launch}
\lstinputlisting[style = code, label = {lst:ex1-localize}, caption = {Ex. 1: 02\_nav\_ex1\_amcl.launch}]{../ex1/02_nav_ex1_amcl.launch}

% Exercise 2
\subsection*{exercise 2}
\label{sec:code-ex2}
\lstinputlisting[style = code, label = {lst:ex2-launch}, caption = {Ex. 2: alice\_nav.launch}]{../ex2/alice_nav_sim/launch/alice_nav.launch}
\lstinputlisting[style = code, label = {lst:ex2-global}, caption = {Ex. 2: global\_costmap\_params}]{../ex2/alice_nav_sim/config/global_costmap_params.yaml}
\lstinputlisting[style = code, label = {lst:ex2-local}, caption = {Ex. 2: local\_costmap\_params}]{../ex2/alice_nav_sim/config/local_costmap_params.yaml}
\lstinputlisting[style = code, label = {lst:ex2-recov}, caption = {Ex. 2: recovery\_behaviors.yaml}]{../ex2/alice_nav_sim/config/recovery_behaviors.yaml}

% Exercise 3
\subsection*{exercise 3}
\label{sec:code-ex3}
\lstinputlisting[style = code, label = {lst:ex3}, caption = {Ex. 3: navigateex3\_0.py}]{../ex3/navigateex3_0.py}
	

\end{document}