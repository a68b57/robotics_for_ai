\documentclass[a4paper,11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{array}
\usepackage{parskip}
\usepackage{float}
\usepackage{tabularx,ragged2e,booktabs,caption}
\usepackage{amsmath}
\usepackage{caption}	% for multiline captions
\usepackage[shortlabels]{enumitem}
\usepackage[breaklinks=true]{hyperref}
\usepackage{listings}
\lstset{
    frame=single,
    breaklines=true,
    basicstyle=\footnotesize\ttfamily,
}



\input{boilerplate.tex}


\begin{document}

\begin{center}
    \huge{Assignment 04 - Object Recognition}\\[1cm]
    \large{Group 01}\\[0.5cm]
    \large{
        Mingcheng Ding (s2948613)\\
        \texttt{dingmingcheng@163.com}
    }\\[0.5cm]
    \large{
        Yannik Beckersjurgen (S2303779)\\
        \texttt{y.beckersjurgen@student.rug.nl}
    }\\[0.5cm]
    \large{
        \textbf{Robotics for AI}\\
        University of Groningen\\[0.5cm]
    {\today}}\\[0.5cm]
\end{center}


% \begin{figure}[!htb]
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{figures/fractal}
% 	\caption{A caption}
% 	\label{fig:fractal}
% \end{figure}

\section*{1 - Creating and Partitioning the Data Set}
\label{sec:ex1}
\subsection*{Data augmentation}
We took approximately 400 photos of each object, keeping the position of the camera and the size of the image (328x436) fixed. The camera had an inclination angle of about 45 degrees and was fixed at the same height as the front Xtion that is used by Alice.

We put the object at the center, the top-left, top-right and bottom of the frame and captured about 100 photos at each position while rotating the object on the disk, in order to get different perspectives. 

For each image, we converted the image to HSV and adjusted the V-channel of the image by 30 to get three brightness levels: dark (-30), light (+30) and normal (no change). For each of these outputs, we rotated the image for 10 degree clockwise and counter-clockwise and also kept a version without any rotation. Then for each of these brightness and rotation combinations, we took several crops from the images by moving the cropping window (70\% of the image size) with a stride of three pixels horizontally and vertically. Again, we also kept a full-sized version of the image. Thus, the augmented data set contains 3 (brightness) x 3 (rotation) x 5 (cropping) = 45 copies for each original image.



\subsection*{Partitioning}
We decided to use 10-fold cross validation to reliably determine the generalization error on the testing set. We opted for k-fold cross validation instead of Monte Carlo cross validation since we augmented the images ourselves before passing them to the Tensorflow training algorithm. Since we partitioned the data into 10 folds before augmentation, we ensured that generated (augmented) images are always in the same set as the original image from which they were generated. Additionally, k-fold cross validation guarantees that each data point is used once in the test set when determining the generalization error.
 
In total, we used 184,095 images for training the neural network. For each of the ten folds, 10 \% of the data were used as the test set. The remaining 90 \% were again split into a training set (90\%) and a validation set (10 \%, used for model selection).
\section*{2 - Training and Testing the Neural Network} 
\label{sec:2}
% Use figures for train/validation set errors, and a table for the best network train/validation/test error.

For training the neural network, we used a constant learning rate $\eta = 0.01$, a training batch size of 128 images, a validation batch size of 500 images and a test batch size of 100 images. The training batch size represents the number of images that are presented to the neural network before updating the weights. We chose this value such that by averaging over several images, we can approximate the true gradient for faster convergence, while keeping the computational cost for each learning step reasonably low. 

We terminate the training after 2000 epochs since as we saw that the model converges rather quickly. While training the network, the validation accuracy is computed repeatedly and after the training， the network which has the lowest validation error is chosen as the final result of the training algorithm.


In \autoref{fig:figure}, we visualize how the training and validation error change as a function of the training step number for the fourth fold. Note that there is a small plateau after about 200 training steps. We observed similar plateaus for most of the k folds. After about 1600 training steps, the increase in accuracy becomes very flat, indicating that terminating the training after 2000 training steps is indeed justified.

\begin{figure}[!htb]
	\centering
 	\includegraphics[width=0.5\textwidth]{../Fold_4.png}
 	\caption{4th fold - accuracy as a function of the training step. Validation error is shown in blue, training error in red.}
 	\label{fig:figure}
\end{figure}

\begin{table}[]
\centering
\caption{k-fold cross validation accuracy}
\label{tab:mainresult}
\begin{tabular}{l|lll}
fold & train & validation & test \\ \hline
1 & 1     & 0.998      & 1    \\
2 & 0.992 & 1          & 0.99 \\
3 & 0.992 & 0.996      & 0.99 \\
4 & 1     & 1          & 1    \\
5 & 0.984 & 1          & 1    \\
6 & 0.977 & 0.998      & 1    \\
7 & 0.992 & 1          & 1    \\
8 & 0.992 & 0.998      & 1    \\
9 & 0.984 & 1          & 0.99 \\
10 & 0.992 & 0.998      & 0.99 \\ \hline
mean &  0.991 & 0.999 & 0.996 \\	
SD &  0.0014 & 0.0072 & 0.0052
\end{tabular}
\end{table}

The results are shown in \autoref{tab:mainresult}. We see that the classification accuracy is very high on average, especially for the validation set. This does not come as a surprise, considering that we selected the network based on validation accuracy. Note that for the test set, we report the accuracy values that were returned by the Python script \verb|retrain2.py|. For the training and validation accuracies, we could use the tensorflow log files to get the accuracies with a larger precision, but since the test accuracy is not added to these, we had to restrict ourselves to two digit precision for the test accuracy for this report. The implications of the very high accuracy on the test sets will be discussed in the next section, when we compare the performance to the classification performance on data generated from the live feed.

\section*{3 - Testing on a Real Live Feed}
To continue with this exercise, we selected the fourth fold form the 10-fold cross validation in the previous step, since it is associated with the best performance across all three sets.

We used the Python script that records rosbag files with a duration of 5 seconds which was provided for this assignment. In order to create a realistic test set, we split the 10 objects into three groups consisting of 3, 3, and 4 objects respectively. For each of these groups, we aligned the objects on the table such that they are on a line that runs perpendicular to the front-to-back axis of the robot. We shifted the position of the objects on the line such that every object is in every position (left, middle, center) exactly once. For each of these permutations, we created three rosbag files by rotating the objects to the left, to the right, and not at all. This gives us 3 (groups) x 3 (positions) x 3 (rotations) = 27 combinations and resulting rosbag files.

We modified the \verb|testImage| script to extract the images which are recognized as containing an object from the rosbag recording. In total, there were 87 recognized clusters. The classification result is shown in \autoref{tab:ex3}, where each row indicates the proportion of occurrences of the object that classified as such (the true positive rate).

\begin{center}
\captionof{table}{Classification accuracy}\label{tab:ex3} 
\begin{tabular} {  c |c }
 \hline
 Object & Accuracy  \\
 \hline
 AADrink  & 0.78   \\
 CocaCola  & 0.25   \\
 Redbull  & 0.875   \\
 Salt  & 0.33   \\
 TomatoSoup  & 0.625   \\
 ChickenSoup  & 0.78   \\
 Shampoo  & 1   \\
 YellowContainer  & 1   \\
 EraserBox  & 0.33   \\
 Pringles  & 1   \\
 Overall  & 0.701   \\
\hline
\end{tabular}
\end{center}

In general, there were more correct classifications than the incorrect cases. Among these 10 objects, shampoo, yellowcontainer and pringles are all classified correctly. The Redbull can also shows a good classification performance with only one mistake. In contrast, we found that 75\% of the Cocacola cans were classified as Redbull. All the misclassifications of the Eraser Box were as Tomatosoup and all the misclassifications of Tomatosoup were as Eraserbox. The salt was mostly misclassified as Redbull or AAdrink. 

Overall, the accuracy was 70 \%. While this shows that the training was effective, we also see that some classes were misclassified most of the time (albeit more than the 10 \% accuracy to be expected by chance).
Still, compared with the results form the real test set (see \autoref{sec:2} 2), the classification accuracy is surprisingly low. Considering that both the validation and the test accuracy were nearly 100 \% in the previous section, we take this as an indication that the data set is not diverse enough to represent all instances occurring in real-life conditions. This seems be true especially for classes that do not have striking characteristics (unlike the Pringles can for example, which was classified correct on all occasions).


 
%\appendix 
%\label{sec:code-ex4}
%\lstinputlisting[style = code, label = {lst:behavior}, caption = {speechrecex3\_0.py}]{../ex3-and-4/speechrecex3_0.py}

\end{document}