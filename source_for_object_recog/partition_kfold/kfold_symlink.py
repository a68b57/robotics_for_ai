import os
import errno


input_folder = '/home/student/Pictures/kfold_images/objects_augmented' 
output_folder = '/home/student/Pictures/kfold_images/final_fold'

fold_to_use_for_testing = 'fold_10'

def make_sure_path_exists(path):
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def get_immediate_subdirectories(a_dir):
	return [name for name in os.listdir(a_dir)
			if os.path.isdir(os.path.join(a_dir, name))]
												

# create test and train directories
train_dir = output_folder + '/' + 'train'
test_dir = output_folder + '/' + 'test'
make_sure_path_exists(train_dir)
make_sure_path_exists(test_dir)

subfolders = get_immediate_subdirectories(input_folder)
for folder in subfolders:
	class_folders = get_immediate_subdirectories(input_folder + '/' + folder)
	for class_folder in class_folders:
		abs_cf_path	= input_folder + '/' + folder + '/' + class_folder
		make_sure_path_exists(train_dir + '/' + class_folder)	
		make_sure_path_exists(test_dir + '/' + class_folder)

		# create a symlink for each input file in the output folder
		imgs = os.listdir(abs_cf_path)
		for img in imgs	:
			abs_img_path = abs_cf_path + '/' + img

			if folder == fold_to_use_for_testing:
				symlink = test_dir + '/' + class_folder + '/' + img
			else: 
				symlink = train_dir + '/' + class_folder + '/' + img
				
			os.symlink(abs_img_path, symlink)
