import os
import errno
from random import shuffle
from shutil import copyfile

input_folder = '/home/student/Pictures/objects_original/nameing'
output_folder = '/home/student/Pictures/kfold_images/objects_partitioned'
modelFullPath = '/home/student/CNN/output_graph.pb'
k_split = 10 


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

# create the k output folders into which data set will be partitioned
for k in range(1, k_split + 1):
    make_sure_path_exists(output_folder + '/' + 'fold_' + str(k))

for subfolder in [x[0] for x in os.walk(input_folder)]:
    if subfolder == input_folder:
        continue

    sf_name = subfolder[len(input_folder)+1:]   # remove absolute path part

    for k in range(1, k_split + 1):
        make_sure_path_exists(output_folder + '/' + 'fold_' + str(k) + '/' + sf_name)

    # get all files in directory
    imgs = os.listdir(subfolder)
    num_imgs = len(imgs)

    # create a random permutation of images in folder
    shuffle(imgs)
    # distribute images over the k-folds (using random selection)
    for i in range(num_imgs):
        output_k = (i % k)  + 1
        output_subfolder = output_folder + '/fold_' + str(output_k) + '/' +  sf_name
        img_name = imgs[i]
        copyfile(subfolder + '/' + img_name, output_subfolder + '/' + img_name) 
