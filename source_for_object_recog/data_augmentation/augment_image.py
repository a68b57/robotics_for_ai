import cv2
import numpy as np



# TODO: maybe add shadow after cropping
def rotate_img(img, degrees):
	print (img.shape)[0:2]
	rows,cols = (img.shape)[0:2]
	M = cv2.getRotationMatrix2D((cols/2, rows/2), degrees, 1)
	dst = cv2.warpAffine(img,M,(cols,rows))
	return dst

# NOTE: y is zero at the top!
# w = width, h = height
def crop_img(img, y, x, w,h):	# coordiantes of upper left and lower right pixels defining the rectangle to be cropped
	print (img.shape)[0:2]
	res = img[y: y + h, x: x + w]
	return res	

def add_shadow(img, delta_v, subtract = False):
	hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	h, s, v = cv2.split(hsv_img)
	old_v = v

	if subtract:
		v -= delta_v
		v[v > 255 - delta_v] = 0		# opencv will start at 255 if value becomes less than 0
	else:
		v += delta_v
		v[v <= delta_v] = 255			# opencv will start at 0 again if value becomes larger than 255.


	res = cv2.merge([h, s, v])
	res = cv2.cvtColor(res, cv2.COLOR_HSV2BGR)
	return res


filepath = '/home/student/Pictures/test/9_group1.jpg'
img = cv2.imread(filepath)

#cv2.imshow("original", img)
#cv2.waitKey(0)

# dst = rotate_img(img, 10)
# cv2.imshow("rotated", dst)
# cv2.waitKey(0)


## show image (middle) and images with changed brightness next to it
# res = add_shadow(img, 80, subtract = False)
# res2 = add_shadow(img, 80, subtract = True)

# temp = np.concatenate((res, img), axis = 1)
# final = np.concatenate((temp, res2), axis = 1)
# cv2.imshow("rotated", final)
# cv2.waitKey(0)

crop1 = crop_img(img, 0, 0, 400, 535)	
cv2.imshow("cropped", crop1)
cv2.waitKey(0)
