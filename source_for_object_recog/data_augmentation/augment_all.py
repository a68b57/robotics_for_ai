import cv2
import numpy as np
import os
from glob import glob
import math
import errno

def rotate_img(img, degrees):
	rows,cols = (img.shape)[0:2]
	M = cv2.getRotationMatrix2D((cols/2, rows/2), degrees, 1)
	dst = cv2.warpAffine(img,M,(cols,rows))
	return dst

# NOTE: y is zero at the top!
# w = width, h = height
def crop_img(img, y, x, w,h):	# coordiantes of upper left and lower right pixels defining the rectangle to be cropped
	res = img[y: y + h, x: x + w]
	return res

# scale = scale of cropping window relative to full image
# stride = step size when moving cropping window across image
def multiple_crops(img, filename, scale, stride):
	# get input dimensions
	height, width = (img.shape)[0:2]

	# start at top left
	w = int(width * scale)
	h = int(height * scale)

	x_end = width - w
	y_end = height - h
	
	xs = list(range(0,x_end,stride))
	ys = list(range(0,y_end,stride))
	for x in xs:
		for y in ys:
			cropped_img = crop_img(img, y,x,w,h)
			out_name = output_dir + "/" + filename + '_crop' + str(x) + '_' + str(y) + '.jpg'
			cv2.imwrite(out_name, cropped_img)
	
	# no crop:
	out_name = output_dir + "/" + filename + '_noCrop' + '.jpg'
	cv2.imwrite(out_name, img)


def change_brightness(img, delta_v, subtract = False):
	hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	h, s, v = cv2.split(hsv_img)
	old_v = v

	if subtract:
		v -= delta_v
		v[v > 255 - delta_v] = 0		# opencv will start at 255 if value becomes less than 0
	else:
		v += delta_v
		v[v <= delta_v] = 255			# opencv will start at 0 again if value becomes larger than 255.


	res = cv2.merge([h, s, v])
	res = cv2.cvtColor(res, cv2.COLOR_HSV2BGR)
	return res


## pipeline: img -> rotate_twice -> add_shadows -> make_crops
def augment_all(img, filename):
	lighter = change_brightness(img, delta_v)
	darker = change_brightness(img, delta_v, subtract = True)
	rotate_twice(img, filename + '_noShadow')
	rotate_twice(lighter, filename + '_light')
	rotate_twice(darker, filename + '_dark')

def rotate_twice(img, filename):
	clockwise = rotate_img(img, rotation_degrees)
	counterclock = rotate_img(img, 360 - rotation_degrees)

	make_crops(clockwise, filename + '_clockRot')
	make_crops(counterclock, filename + '_couterRot')
	make_crops(img, filename + '_noRot')

def make_crops(img, filename):
	multiple_crops(img, filename, crop_scale, crop_stride)


## =============================== parameters =================================
rotation_degrees = 10
delta_v = 30 # change in brightness
crop_scale = 0.7
crop_stride = 70

output_dir = None		# GLOBAL, don't set this
input_files = None		# GLOBAL, don't set this

output_base_path = "/home/student/Pictures/kfold_images/objects_augmented/fold_10"			    # set these
input_base_path = "/home/student/Pictures/kfold_images/objects_partitioned/fold_10"


# for every input directory
for directory in os.listdir(input_base_path):
	os.chdir(input_base_path)
	if not os.path.isdir(directory):
		continue

	# create output directory
	output_dir = output_base_path + '/' + directory
	try:	
		os.makedirs(output_dir)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise
	
	input_files = input_base_path + '/' + directory
	os.chdir(input_files)
	
	# for every input picture
	for file in os.listdir(input_files):	
		if not file.endswith(".jpg"):
			continue
		print file
		img = cv2.imread(file)
		filename = file[0:-4]	# remove .jpg from filename
		
		# rotates, adds shadows, crops and saves results
		augment_all(img, filename)