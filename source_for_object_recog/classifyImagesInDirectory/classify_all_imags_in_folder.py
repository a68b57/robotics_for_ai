import os
import errno
import cv2
import numpy as np
import tensorflow as tf
from PIL import Image
from classifyImage import run_inference_on_image
from classifyImage import create_graph

input_folder = '/home/student/Pictures/rosbag_imgs/static'
modelFullPath = '/home/student/group01-Yannik-Michael/Network1_fullSet/output_graph.pb'
labelsFullPath = '/home/student/group01-Yannik-Michael/Network1_fullSet/output_labels.txt'


imgs = os.listdir(input_folder)
print len(imgs)

i = 1
create_graph()
for img in imgs :
	image = cv2.imread(input_folder + '/' + img)
	print str(i)
	i = i + 1
	print run_inference_on_image(input_folder + '/' + img)
	cv2.imshow('image', image)
	cv2.waitKey(0)


