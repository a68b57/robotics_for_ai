import numpy as np
import tensorflow as tf
import cv2
from PIL import Image

# NOTE: THIS FILE IS ADAPTED FROM testCNN in the alice_object/scripts folder


def create_graph():
    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    with tf.gfile.FastGFile(modelFullPath, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')
        
def run_inference_on_image(imagePath):
    answer = None

    if not tf.gfile.Exists(imagePath):
        tf.logging.fatal('File does not exist %s', imagePath)
        return answer

    img = cv2.imread(imagePath);
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB);
    #img = Image.open(imagePath);
    #img = cv2.resize(img, (h,w));
    
    image_data = tf.gfile.FastGFile(imagePath, 'rb').read()   
    
    
    # Creates graph from saved GraphDef.
    # create_graph() # UNCOMMENT THIS

    with tf.Session() as sess:

        softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
                
        predictions = sess.run(softmax_tensor,
                               {'DecodeJpeg:0': img})
        
        #predictions = sess.run(softmax_tensor,
        #                       {'Cast:0': img})
        predictions = np.squeeze(predictions)

        top_k = predictions.argsort()[-5:][::-1]  # Getting top 5 predictions
        f = open(labelsFullPath, 'rb')
        lines = f.readlines()
        labels = [str(w).replace("\n", "") for w in lines]
        for node_id in top_k:
            human_string = labels[node_id]
            score = predictions[node_id]
            print('%s (score = %.5f)' % (human_string, score))

        answer = labels[top_k[0]]
        return answer


if __name__ == '__main__':
    run_inference_on_image()

